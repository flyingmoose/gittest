FROM alpine:latest

RUN apk add --no-cache python3 py-pip
COPY app.py /app/app.py

CMD ["python", /app/app.py]